from torch_geometric.data import Data


class LineGraphPairData(Data):
    def __init__(self,
        edge_index_G,
        x_atm,
        x_bnd,
        edge_index_L = None,
        x_ang        = None,
        mask_dih_ang = None,
    ):
        super().__init__()
        self.edge_index_G = edge_index_G
        self.edge_index_L = edge_index_L
        self.x_atm = x_atm
        self.x_bnd = x_bnd
        self.x_ang = x_ang
        self.mask_dih_ang = mask_dih_ang
        
    def __inc__(self, key, value):
        if key == 'edge_index_G':
            return self.x_atm.size(0)
        if key == 'edge_index_L':
            return self.x_bnd.size(0)
        else:
            return super().__inc__(key, value)
