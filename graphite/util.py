import numpy as np


__all__ = ['np_scatter', 'encode_labels']


def np_scatter(src, index, func=None):
    """Abstraction of the `torch_scatter.scatter` function.
    See https://pytorch-scatter.readthedocs.io/en/latest/functions/scatter.html
    for how `scatter` works in PyTorch.

    Args:
        src (list): The source array.
        index (list of ints): The indices of elements to scatter.
        func (function, optional): Function that operates on elements with the same indices.
            If this function is `None`, then `src` is simply scattered into groups of elements with the same indices.

    :rtype: generator
    """
    src, index = np.array(src), np.array(index)
    if func:
        return (func(src[index == i]) for i in np.unique(index))
    else:
        return (src[index == i] for i in np.unique(index))


def encode_labels(labels):
    """Encode categorical labels into integer representation.
    For example, [4, 5, 10, 5, 4, 4] would be encoded to [0, 1, 2, 1, 0, 0],
    and ['paris', 'paris', 'tokyo', 'amsterdam'] would be encoded to [1, 1, 2, 0].
    """
    _, labels = np.unique(labels, return_inverse=True)
    return labels
