import torch
from torch.nn      import Embedding, Linear, ModuleList, SiLU, Sequential, Softplus
from torch_scatter import scatter
from ..basis       import RadialBesselFunc
from ..conv        import EGConv


class ALIGNN_interpretable(torch.nn.Module):
    """ALIGNN model that uses auxiliary line graph to explicitly represent and encode bond angles.
    This version is designed to be "interpretable" by eventually summing all graph components,
    each of which holds a nonnegative scalar at the last layer,
    into a positive scalar quantity for final prediction.

    Reference: https://www.nature.com/articles/s41524-021-00650-1.
    """
    def __init__(self, dim=100, num_interactions=6, num_species=3, cutoff=3.0):
        super().__init__()

        self.dim              = dim
        self.num_interactions = num_interactions
        self.cutoff           = cutoff

        self.embedding        = Embedding(num_species, dim)
        self.expand_bnd_dist  = RadialBesselFunc(dim, cutoff)
        self.expand_ang       = RadialBesselFunc(dim, 2.0)

        self.atm_bnd_interactions = ModuleList()
        self.bnd_ang_interactions = ModuleList()
        for _ in range(num_interactions):
            self.atm_bnd_interactions.append(EGConv(dim, dim))
            self.bnd_ang_interactions.append(EGConv(dim, dim))

        self.head_atm = Sequential(
            Linear(dim, 3), Softplus(),
        )
        self.head_bnd = Sequential(
            Linear(dim, 3), Softplus(),
        )
        self.head_ang = Sequential(
            Linear(dim, 3), Softplus(),
        )

        self.reset_parameters()

    def reset_parameters(self):
        self.embedding.reset_parameters()
        for i in range(self.num_interactions):
            self.atm_bnd_interactions[i].reset_parameters()
            self.bnd_ang_interactions[i].reset_parameters()

    def embed_angles(self, x_ang):
        cos_ang = torch.cos(torch.deg2rad(x_ang)) + 1.00001
        return self.expand_ang(cos_ang)

    def forward(self, data):
        edge_index_G = data.edge_index_G
        edge_index_L = data.edge_index_L
        h_atm        = self.embedding(data.x_atm)
        h_bnd        = self.expand_bnd_dist(data.x_bnd)
        h_ang        = self.embed_angles(data.x_ang)

        for i in range(self.num_interactions):
            h_bnd, h_ang = self.bnd_ang_interactions[i](h_bnd, edge_index_L, h_ang)
            h_atm, h_bnd = self.atm_bnd_interactions[i](h_atm, edge_index_G, h_bnd)

        h_atm = self.head_atm(h_atm)
        h_bnd = self.head_bnd(h_bnd)
        h_ang = self.head_ang(h_ang)

        h_atm_agg = scatter(h_atm, data.x_atm_batch, dim=0, reduce='add')
        h_bnd_agg = scatter(h_bnd, data.x_bnd_batch, dim=0, reduce='add')
        h_ang_agg = scatter(h_ang, data.x_ang_batch, dim=0, reduce='add')

        out = h_atm_agg + h_bnd_agg + h_ang_agg
        return out, (h_atm, h_bnd, h_ang)

    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'dim={self.dim}, '
                f'num_interactions={self.num_interactions}, '
                f'cutoff={self.cutoff})')
