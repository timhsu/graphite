import torch
import numpy as np
import itertools
from functools import partial
from ase.neighborlist import neighbor_list
from torch_geometric.data import Data
from .util import np_scatter


__all__ = [
    'atoms2graph',
    'atoms2pygdata',
    'line_graph',
    'line_graph_dih_ang',
    'get_bnd_angs',
    'get_dih_angs',
]


def atoms2graph(atoms, cutoff, edge_dist=False):
    """Convert an ASE `Atoms` object into a graph.
    Returns the graph (in COO format),
    its node attributes (as a 1D array of atom types, `z`),
    and its edge attributes (format determined by `edge_dist`).

    Args:
        atoms (ase.Atoms): Collection of atoms to be converted to a graph.
        cutoff (float): Cutoff radius for nearest neighbor search.
        edge_dist (bool, optional): Set to `True` to output edge distances.
            Otherwise, output edge vectors.

    :rtype: (ndarray, ndarray, ndarray)
    """
    _, x = np.unique(atoms.numbers, return_inverse=True)
    i, j, D = neighbor_list('ijD', atoms, cutoff)
    edge_index = np.stack((i, j))
    if edge_dist:
        D = np.linalg.norm(D, axis=1, keepdims=True)
    return edge_index, x, D


def atoms2pygdata(atoms, cutoff, edge_dist=False):
    """Convert an ASE `Atoms` object into a PyG `Data` graph object.
    Returns the graph holding `x`, `edge_index`, and `edge_attr`.

    Args:
        atoms (ase.Atoms): Collection of atoms to be converted to a graph.
        cutoff (float): Cutoff radius for nearest neighbor search.
        edge_dist (bool, optional): Set to `True` to store edge distances as `edge_attr`.
            Otherwise, store edge vectors.

    :rtype: torch_geometric.data.Data
    """
    edge_index, x, edge_attr = atoms2graph(atoms, cutoff, edge_dist)
    data = Data(
        x          = torch.tensor(x,          dtype=torch.long),
        edge_index = torch.tensor(edge_index, dtype=torch.long),
        edge_attr  = torch.tensor(edge_attr,  dtype=torch.float),
    )
    return data


permute_2 = partial(itertools.permutations, r=2)
def line_graph(edge_index_G):
    """Return the (angular) line graph of the input graph.

    Args:
        edge_index_G (ndarray): Input graph in COO format.
    """
    src_G, dst_G = edge_index_G
    edge_index_L = [
        (u, v)
        for edge_pairs in np_scatter(np.arange(len(dst_G)), dst_G, permute_2)
        for u, v in edge_pairs
    ]
    return np.array(edge_index_L).T


def line_graph_dih_ang(edge_index_G):
    """Return the "dihedral angle line graph" of the input graph.

    Args:
        edge_index_G (ndarray): Input graph in COO format.
    """
    src, dst = edge_index_G
    edge_index_L = [
        (u, v)
        for i, j in edge_index_G.T
        for u in np.flatnonzero((dst == i) & (src != j))
        for v in np.flatnonzero((dst == j) & (src != i))
    ]
    return np.array(edge_index_L).T


def get_bnd_angs(atoms, edge_index_G, edge_index_L_bnd_ang, cos_ang=False):
    """Return the bond angles for the (angular) line graph edges.
    """
    indices  = edge_index_G.T[edge_index_L_bnd_ang.T].reshape(-1, 4)
    bnd_angs = atoms.get_angles(indices[:, [0, 1, 2]]).reshape(-1, 1)
    if cos_ang:
        return np.cos(np.radians(bnd_angs))
    else:
        return bnd_angs


def get_dih_angs(atoms, edge_index_G, edge_index_L_dih_ang, cos_ang=False):
    """Return the dihedral angles for the dihedral line graph edges.
    """
    indices  = edge_index_G.T[edge_index_L_dih_ang.T].reshape(-1, 4)
    dih_angs = atoms.get_dihedrals(indices[:, [0, 1, 3, 2]]).reshape(-1, 1)
    if cos_ang:
        return np.cos(np.radians(dih_angs))
    else:
        return dih_angs
