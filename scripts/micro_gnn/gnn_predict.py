import torch
import numpy as np
import ase.io
from torch_geometric.data import Data

import os
import sys; sys.path.append(os.path.join(sys.path[0], '../..'))
from graphite           import atoms2graph
from graphite.nn.models import EGCONV_GNN

from cmdline_args       import parse_cmdline


if __name__ == '__main__':
    # ----------------------------------------------------------------------------------
    # Input parameters
    args =  parse_cmdline()

    FNAME      = args.filename
    CHECKPOINT = args.checkpoint


    # ----------------------------------------------------------------------------------
    # Convert atoms to graph
    atoms = ase.io.read(FNAME)
    print(atoms)

    edge_index, edge_attr = atoms2graph(atoms, cutoff=3.5)
    x    = np.zeros((len(atoms), ))
    data = Data(
        x          = torch.tensor(x,          dtype=torch.long),
        edge_index = torch.tensor(edge_index, dtype=torch.long),
        edge_attr  = torch.tensor(edge_attr,  dtype=torch.float),
    )
    print(data)

    import gc
    del edge_index
    del edge_attr
    del x
    gc.collect()

    print(f'Number of nodes:         {data.num_nodes}')
    print(f'Number of edges:         {data.num_edges}')
    print(f'Average node degree:     {data.num_edges / data.num_nodes:.2f}')
    print(f'Contains isolated nodes: {data.contains_isolated_nodes()}')
    print(f'Contains self-loops:     {data.contains_self_loops()}')
    print(f'Is undirected:           {data.is_undirected()}')


    # ----------------------------------------------------------------------------------
    # Load saved model weights
    model = EGCONV_GNN(dim=100, num_interactions=3, num_species=1, cutoff=3.5)
    checkpoint = torch.load(CHECKPOINT)
    model.load_state_dict(checkpoint['model_state_dict'])
    print(model)


    # ----------------------------------------------------------------------------------
    # Apply model and save
    model = model.to('cpu')
    model.eval()

    pred = model(data).detach().numpy()

    atoms.info['cutoff'] = 3.5
    atoms.arrays['orderness'] = pred.reshape(-1)
    
    save_fname = FNAME + '.extxyz'
    ase.io.write(save_fname, atoms, format='extxyz')
    print(f'Model output saved to {save_fname}')
