import numpy as np
import torch
from sklearn.model_selection    import train_test_split
from torch_geometric.loader     import DataLoader
from pathlib                    import Path
from time                       import perf_counter
from datetime                   import datetime

import os
import sys; sys.path.append(os.path.join(sys.path[0], '../../..'))
from graphite.nn.models         import E3NN_NequIP

from cmdline_args               import parse_args


if __name__ == '__main__':
    # ----------------------------------------------------------------------------------
    # Read command line arguments
    args =  parse_args()

    DATASET       = args.dataset
    LOG_PATH      = args.log_path
    RUN_ID        = args.run_id
    CUTOFF        = args.cutoff
    NUM_CONVS     = args.num_convs
    LEARN_RATE    = args.learn_rate
    EPOCHS        = args.epochs
    BATCH_SIZE    = args.batch_size
    IRREPS_HIDDEN = args.irreps_hidden
    NUM_NEIGHBORS = args.num_neighbors

    # ----------------------------------------------------------------------------------
    # Initiate model
    model = E3NN_NequIP(
        irreps_in      = '64x0e',
        irreps_hidden  = IRREPS_HIDDEN,
        irreps_out     = '4x0e',
        irreps_node    = '1x0e',
        irreps_edge    = '1x0e + 1x1e + 1x2e',
        num_convs      = NUM_CONVS,
        num_neighbors  = NUM_NEIGHBORS,
    )
    print('Model:')
    print(f'  {model}')

    # ----------------------------------------------------------------------------------
    # Load data into loaders
    dataset = torch.load(DATASET)
    ds_train, ds_valid = train_test_split(dataset, train_size=0.9, random_state=12345)
    loader_train = DataLoader(ds_train, batch_size=BATCH_SIZE, shuffle=True)
    loader_valid = DataLoader(ds_valid, batch_size=BATCH_SIZE, shuffle=False)
    print(f'Number of train graphs: {len(loader_train.dataset)}')
    print(f'Number of valid graphs: {len(loader_valid.dataset)}')

    # ----------------------------------------------------------------------------------
    # Prepare for training
    def softCrossEntropy(input, target):
        logprobs = torch.nn.functional.log_softmax(input, dim=1)
        return -(target * logprobs).sum() / input.shape[0]


    optimizer = torch.optim.Adam(model.parameters(), lr=LEARN_RATE)
    # scheduler = torch.optim.lr_scheduler.OneCycleLR(
    #     optimizer,
    #     max_lr          = LEARN_RATE*10,
    #     steps_per_epoch = len(loader_train),
    #     epochs          = EPOCHS,
    # )
    device    = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model     = model.to(device)
    loss_fn   = softCrossEntropy
    print(f'Training on {device}')


    def train(loader):
        model.train()
        total_loss = 0.0
        for data in loader:
            optimizer.zero_grad()
            data = data.to(device)
            loss = loss_fn(model(data), data.y)
            loss.backward()
            optimizer.step()
            # scheduler.step()
            total_loss += loss.item()
        return total_loss / len(loader)


    @torch.no_grad()
    def test(loader):
        model.eval()
        total_loss = 0.0
        for data in loader:
            data = data.to(device)
            loss = loss_fn(model(data)[1], data.y)
            total_loss += loss.item()
        return total_loss / len(loader)

    # ----------------------------------------------------------------------------------
    # Train!
    L_train, L_valid, T_perf = [], [], []
    for epoch in range(EPOCHS):
        time_0     = perf_counter()
        loss_train = train(loader_train)
        time_i     = perf_counter() - time_0
        loss_valid = test(loader_valid)
        
        L_train.append(loss_train)
        L_valid.append(loss_valid)
        T_perf.append(time_i)
        
        print(
            f'{epoch:>4d} | '
            f'loss_train: {loss_train:>10.5f} | '
            f'loss_valid: {loss_valid:>10.5f} | '
            f'time: {time_i:>8.4f}'
        )

    # ----------------------------------------------------------------------------------
    # Save model and train history
    dir_name = Path(LOG_PATH)/RUN_ID
    if not os.path.exists(dir_name): os.makedirs(dir_name)

    now = datetime.now().strftime('%Y%m%d-%H%M%S')

    torch.save(model, str(dir_name/f'{now}-model.pt'))

    np.savetxt(
        str(dir_name/f'{now}-train-hist.txt'),
        np.array((L_train, L_valid, T_perf)).T,
        fmt='%.8f',
        header='L_train L_valid T_perf',
    )

    print('Model and training history have been logged. Good bye.')
