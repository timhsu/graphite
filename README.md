# graphite

A library for developing Graph Neural Networks (GNNs) based on atomic structures.


## Requirements

* Atomic Simulation Environment (`ase`)
    * For reading atomic structures and computing the neighbor list (used to generate graphs).
* PyTorch-Geometric (PyG)
    * For implementing GNN operations and models.
    * PyG depends on PyTorch (1.8.1 or newer is preferred).
* Euclidean neural networks (`e3nn`)
    * For implementing *equivariant* networks via tensor products of irreducible representations.

I suggest using conda to first install `ase` and PyTorch 1.8.1+, then install PyG, then use pip to install `e3nn`.


## Implemented works

* ALIGNN
    * [*Atomistic Line Graph Neural Network for improved materials property predictions*][ALIGNN paper]
* ALIGNN-d
    * [*Efficient, Interpretable Graph Neural Network Representation for Angle-dependent Properties and its Application to Optical Spectroscopy*][ALIGNN-d paper]
* Edge-gated graph convolution
    * [*Benchmarking Graph Neural Networks*][Edge-gated conv paper]
* Simple equivariant convolution
    * [e3nn documentation][e3nn basic conv doc]
* NequIP
    * [*E(3)-Equivariant Graph Neural Networks for Data-Efficient and Accurate Interatomic Potentials*][NequIP paper]
* Equivariant self-attention
    * [e3nn documentation][e3nn transformer doc]
    * [*SE(3)-Transformers: 3D Roto-Translation Equivariant Attention Networks*][SE(3)-transformer paper]


## Usage

The purpose of `graphite` is
* to process/analyze atomic structures in the context of GNNs,
* and to define (graph) neural network operations or graph data formats not implemented in PyTorch and PyG.

Therefore, `graphite` is meant to be a general toolbox of GNN codes (e.g., helper functions and custom graph convolutions) for atomic structures. Production codes for specific applications should be hosted elsewhere.

* The `graphite` folder contains the source code.
* The `scripts` folder contains template scripts for running or training models defined in `graphite/nn/models`.


### Examples

#### Convert atomic structures to PyG's `Data` object using ASE's neighbor list algorithm

```python
import torch
import ase.io
from graphite import atoms2graph
from torch_geometric.data import Data

atoms = ase.io.read('path/to/atoms.file')

# Build a graph based on a cutoff radius of 3.5 angstroms.
edge_index, x_atm, edge_attr = atoms2graph(atoms, cutoff=3.5)

# `edge_index` is the graph connectivity in COO format.

# `x_atm` is the input features for the atoms.
# This can be an array of N x M, where N is the number of atoms and M is the number of channels.
# But in this case, this array is N-dimensional, where each element is an integer denoting the atom type.

# `edge_attr` is the input features for the edges.
# This can be an array of E x C, where E is the number of edges and C is the number of channels.
# But in this case, this array is E x 3, where each row is the edge/bond vector. 

# Finally, construct the PyG `Data` object that combines the graph, the node features, and the edge features.
data = Data(
    x          = torch.tensor(x_atm,      dtype=torch.float),
    edge_index = torch.tensor(edge_index, dtype=torch.long),
    edge_attr  = torch.tensor(edge_attr,  dtype=torch.float),
)
```


#### Save PyG graphs into a PyTorch .pt file

[As documented in the PyG website][PyG dataset doc], the simplest form of a PyG dataset is a plain Python list containing PyG `Data` objects (aka graphs). Below is a data processing example that reads an MD trajectory file, convert each snapshot into PyG `Data`, and save the snapshots into a PyTorch .pt file.

```python
import torch
import ase.io
from graphite import atoms2pygdata

traj = ase.io.read('path/to/trajectory.file', index=':')
dataset = [atoms2pygdata(atoms) for atoms in traj]
torch.save(dataset, 'path/to/savefile.pt')
```


#### Line graph construction

```python
from graphite import line_graph, line_graph_dih_ang

edge_index_G = ... # Define your original graph G in terms of `edge_index` (this COO format is used by PyG)

# Construct the corresponding line graph L for bond angles
edge_index_L_bnd_ang = line_graph(edge_index_G)

# Construct the corresponding "line graph" L' (not technically a line graph) for dihedral angles
edge_index_L_dih_ang = line_graph_dih_ang(edge_index_G)
```


[ALIGNN paper]: https://www.nature.com/articles/s41524-021-00650-1
[ALIGNN-d paper]: https://arxiv.org/abs/2109.11576
[Edge-gated conv paper]: https://arxiv.org/abs/2003.00982
[e3nn basic conv doc]: https://docs.e3nn.org/en/stable/guide/convolution.html
[NequIP paper]: https://arxiv.org/abs/2101.03164
[SE(3)-transformer paper]: https://proceedings.neurips.cc/paper/2020/hash/15231a7ce4ba789d13b722cc5c955834-Abstract.html
[e3nn transformer doc]: https://docs.e3nn.org/en/stable/guide/transformer.html
[PyG dataset doc]: https://pytorch-geometric.readthedocs.io/en/latest/notes/create_dataset.html
